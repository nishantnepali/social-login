<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [//change to any provider
        'client_id' => '163545897558505',
        'client_secret' => 'e2a5bb91641ce8285cf33a76c93fa92d',
        'redirect' => 'http://localhost:8000/auth/facebook/callback',
    ],
    'twitter' => [//change to any provider
        'client_id' => '1U7BLfHwDpGv3QJS99dTS3XyP',
        'client_secret' => 'H2debI87CCxwCW202Vit4joGTdHyk8cuyQOk2X7rg6sDQKkULV',
        'redirect' => 'http://localhost:8000/auth/twitter/callback',
    ],
    'google' => [//change to any provider
        'client_id' => '312964447748-lcse152cbfed5lp2rhkhc3u9ij8q2tsf.apps.googleusercontent.com',
        'client_secret' => 'froqXrIhkpTtqP5dElPrgai7',
        'redirect' => 'http://localhost:8000/auth/google/callback',
    ],

];
